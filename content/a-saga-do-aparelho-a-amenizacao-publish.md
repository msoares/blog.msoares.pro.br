Title: A Saga do Aparelho - A Amenização
Author: milton <milton.soares.filho@gmail.com>
Date: 2007-01-19 15:35:27

Larguei os bets em Canoa Quebrada, resolvi fazer a barba com lâmina cega mesmo.
Fogo é aguentar a coceirada no dia seguinte, ainda mais pra mim que já não tenho
uma pele muito boa. Confira o antes e depois nas fotos abaixo.

Â <a class="imagelink" title="ANTES" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/01/img-015.jpg"><img id="image11" height="96" alt="ANTES" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/01/img-015.thumbnail.jpg" /></a> <a class="imagelink" title="DEPOIS" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/01/img-016.jpg"><img id="image12" height="96" alt="DEPOIS" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/01/img-016.thumbnail.jpg" /></a>

Já cheguei em Recife/PE e nada da bendita lâmina. Começo a achar que só em
Taiwan pra encontrá-la. Paciência...
