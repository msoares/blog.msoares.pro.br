Title: A Saga do Aparelho de Barbear
Author: milton <milton.soares.filho@gmail.com>
Date: 2007-01-01 19:30:15

Ano retrasado comprei um aparelho de barbear elétrico da Philips (philipshave
ph-444) pra tentar me livrar do domínio maléfico que a gillette (tm) exercia em
minha vida. Comprar refil de lâmina manual a cada duas barbeadas estava ficando
muito chato e muito caro também. Sem falar que apertando a distribuição das
lâminas de modelos mais antigos, a empresa praticamente obrigava os usuários a
comprarem modelos mais novos (mais caros também).

Após um investimento razoável - uns duzentos reais na época -, passei seis meses
maravilhado com a maquininha. Simples, limpo, prático e deixava uma textura
agradável (ao menos não recebia muitas reclamações, hehehe).

Agora vem a armadilha manjada que caí de novo. A lâmina do aparelho elétrico não
é eterna. Dura bastante, mas eventualmente deve ser substituída. Nesse ponto eu
me danei, pois não encontrei uma loja em Manaus que vendesse a bendita. Em Belém
não consegui procurar e em São Luís nada até agora.

Vamos ver até que estado terei que descer pra encontrar essa coisa. Enquanto
isso vou deixando a barba crescer. Azar de quem me vê com essa cara de ogro.

PS: outra hora posto uma foto do meu estado lamentável.
