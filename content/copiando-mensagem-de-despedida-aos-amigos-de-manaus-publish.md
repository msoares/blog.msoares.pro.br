Title: Copiando mensagem de despedida aos amigos de Manaus
Author: milton <milton.soares.filho@gmail.com>
Date: 2006-12-11 15:40:47

Odeio o esquema de mensagem do Orkut, queria poder usá-lo como bem entendesse,
mandando CC/BCC pra e-mails aleatórios e sem aqueles cabeçalhos chatos que
poluem o texto. Mas como foi só lá que organizei uma lista de amigos de Manaus,
acabei enviando a mensagem de despedida desse jeito mesmo.

Copio aqui para fins históricos.

<blockquote>
<strong>Assunto: Até a Próxima</strong>

Na teoria, essa será minha última semana aqui em Manaus.

Quero deixar um grande abraço a todos vocês que conheci aqui nessa cidade
maravilhosa e a certeza de que só terei boas palavras pra me referir sobre este
lugar e, principalmente, sobre suas pessoas.

Obrigado por todos os momentos compartilhados, pelas grandes experiências e pelo
tanto que me passaram/ensinaram, cada um do seu jeito. Espero poder retribuir
algum dia, de alguma maneira.

Manaus valeu a pena por cada momento, sem exceção. Conquistou um pedaço do meu
coração que nunca conseguirei mover dessas terras.

Sucesso e um ótimo fim de ano para todos.

[]s, milton

Contatos:
E-mail: <a target="_blank" href="mailto:milton.soares.filho@gmail.com">mailto:milton.soares.filho@gmail.com</a>
Weblog: <a target="_blank" href="http://msoares.dreamhosters.com/">http://msoares.dreamhosters.com</a>

PS: mais tarde mando o endereco do blog onde pretendemos registrar a viagem
desde Manaus até o extremo sul do país.
</blockquote>

O título é "Até a próxima" porque a gente nunca sabe o que pode acontecer...
