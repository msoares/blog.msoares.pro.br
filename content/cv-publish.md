Title: CV
Author: milton <milton.soares.filho@gmail.com>
Date: 2006-12-14 18:22:13

<pre>
<b>Milton Soares Filho</b>
Brazilian, 27 years old, single, living at Curitiba/Paraná
<em>milton.soares.filho@gmail.com (e-mail), milton.soares.filho (skype)</em>

<b>Interests</b>

Work on adapting linux for embedded environments and developing applications
for portable devices, using agile methods and tools.

<b>Skills</b>

- Experienced C systems programmer (ANSI, POSIX)
- Unix-like systems application development (6+ years)
- Intermediary knowledge on Python and C++ development
- Fluent English speaker and writer
- Intermediary experience on Java programming
- Skilled on shell scripting programming
- Revision control systems user for several years (Subversion, SVK, git, CVS)
- TCP/IP networking
- Familiar with web-services technologies (SOAP toolkits, WSDL)

<b>Education</b>

B.S in Computer Science at Universidade Federal do Paraná (UFPR), Brazil
[1998-2002]

Electronics Technician Assistant at Centro Federal de Educação Tecnológica do
Paraná (CEFET-PR), Brazil [1994-1998]

Spanish conversation and grammar course at Centro de Idiomas Jardim das
Américas, Brazil [2002-2004]

English conversation and grammar course at Wizard Jardim das Américas, Brazil
[1998-2002]

<b>Professional Experience</b>

<em>Research and Development, Mandriva/Conectiva (August, 2004 ~ October, 2006) </em>

  Senior developer on BenQ-Siemens LinuX Mobile Phone Manager (XMPM) project.
  Worked on low-level components, including implementation of communication
  protocols such as SyncML, ObeX and AT commands. Accomplished integration with
  higher-level layers, such as Java components by using web-service technologies
  (SOAP toolkits). Also worked on internationalization (i18n) and installation
  features, both on script and python-based components. Experience on managing
  distribution-specific issues and fine-tuning kernel USB-Serial modules.

  Technical leader on XMPM2 project. Succeeded to introduce Mandriva Java
  skills to the project by implementing prototype components for SMS management
  (GSM AT commands), using Java, JNI and RxTx features.

<em>Research and Development, Lactec (December, 2002 ~ August, 2004)</em>

  Project, development and maintenance of Copel's (Companhia Paranaense de
  Energia) substations automation systems. Mainly focused on integration of
  newer communication protocols such as DNP (Distributed Network Protocol) and
  IEC 60870-5. Created components were targeted to QNX, a posix-compliant
  operating system with real-time constraints.

  Usage of modelling languages such as UML and SDL-RT (specification and
  Description Language - Real Time Extensions) and contact with CVS and Borland
  Project assistance tools (StarTeam).

<em>South-cone Procurement Department, Exxonmobil Brasil S.A. (March, 2002 ~ December, 2002)</em>

  Trainee on development of practical solutions for office requirements.
  Usage of MS office tools (Excel, Word and Access), VB programming and minor
  developments for intranet sites. Also contact with SAP R/3 integrated
  management tool.

<em>Research and Development, Lactec (October, 2001 ~ March, 2002)</em>

  Trainee. Development and maintenance of automation related communication
  protocols. First contact with Copel automation systems and QNX platform.

<em>IT, Gracco Consultoria em Informática (October, 2000 ~ October, 2001)</em>

  Trainee. Website development and maintenance, supervisioned by Daniel
  Wandarti Filho. Contact with web related technologies such as Java, JSP,
  Apache Server, Jakarta-Tomcat, Servlets, HTML, XML, XSL, XSLT, XPATH, CSS
  and javascript.

<b>Miscellaneous</b>

- Co-author of Sniffdet, an open-source remote sniffers detector for TCP/IP
  networks (http://sniffdet.sourceforge.net)
- Motorcycle riding (http://www.rockpesado.com.br)
- Blogger wannabe (http://msoares.dreamhosters.com)
- Video-game hobbyist
</pre>