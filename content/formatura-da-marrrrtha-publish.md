Title: Formatura da Marrrrtha
Author: milton <milton.soares.filho@gmail.com>
Date: 2007-02-13 18:02:47

Passei uma semana em Curitiba pra aproveitar a formatura da minha irmã, agora
uma Turismóloga (ou turista, nunca sei direito, hehehe) capacitada pela <a
href="http://www.faculdadescuritiba.br/centro.asp?menu=1,1">Faculdades Integradas Curitiba</a>.

A colação aconteceu no dia 01Fev, no anfiteatro do campus da rua Chile. Pra
variar foi massante, mas valeu pra rever parentes a muito sumidos (meu caso,
principalmente), presenciar o discurso de agradecimento aos ausentes proferido
pela minha irmã (arranjamos até um <a
href="http://youtube.com/watch?v=Fr3gL5D8V50">vídeo pirata</a> dele) e tomar uns
goles na cachaçaria depois da cerimônia.

Já no baile a coisa foi diferente. Com carta branca do meu pai (hmm, carta cinza
na verdade, mas não forcei a vista pra ver a diferença), desci três litros de
Red Label em comemoração, um deles antes mesmo de sair de casa. O povo se
divertiu demais! A noite inteira zoando com a família, com o Dinho Ouro Preto,
namorado da minha irmã e com os poucos amigos que compareceram. Ganhei até um
autógrafo no peito da celebridade presente.

Saímos do salão do clube Santa Mônica perto das seis da madrugada, com a firme
intenção de tomarmos café-da-manhã juntos num tal de Babilônia. É claro que
ninguém acertou o caminho. Teve neguinho que saiu do carro no meio da rua pra
fechar a janela do outro lado. Até agora tento me lembrar como cheguei em casa,
mas só recordo até o momento que larguei a Kaysa em casa (sim, eu estava
dirigindo).

<a title="Autógrafo do Dinho Ouro Preto" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/autografo.jpg"><img alt="Autógrafo do Dinho Ouro Preto" id="image16" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/autografo.thumbnail.jpg" /></a> <a title="Foto pros fãs-cunhados" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/dinho.jpg"><img alt="Foto pros fãs-cunhados" id="image17" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/dinho.thumbnail.jpg" /></a>

<a title="Todos os filhos do meu pai" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/familia.jpg"><img alt="Todos os filhos do meu pai" id="image18" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/familia.thumbnail.jpg" /></a> <a title="Irmãos exo-uterinos" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/irmaos.jpg"><img alt="Irmãos exo-uterinos" id="image19" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/irmaos.thumbnail.jpg" /></a>

<a title="O resto da família Soares (primo, irmã, irmão e pai galante)" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/kako_pai.jpg"><img alt="O resto da família Soares (primo, irmã, irmão e pai galante)" id="image20" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/kako_pai.thumbnail.jpg" /></a> <a title="Olha as primas aí" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/primas.jpg"><img alt="Olha as primas aí" id="image25" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/primas.thumbnail.jpg" /></a> <a title="Professor da Marrrtha" class="imagelink" href="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/prof_malucao.jpg"><img alt="Professor da Marrrtha" id="image22" src="http://www.msoares.dreamhosters.com/wp-content/uploads/2007/02/prof_malucao.thumbnail.jpg" /></a>

Parabéns, mana, muita sorte pra você nessa nova etapa de sua vida.
