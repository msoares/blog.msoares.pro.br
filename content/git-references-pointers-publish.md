Title: Git References' Pointers
Author: milton <milton.soares.filho@gmail.com>
Date: 2009-03-25 08:47:39

Earlier this year I've spent some time gathering references to study Git, the
version control system everybody seems to love nowadays.

Thankfully, someone took the time to bind several useful links in one central
webpage. If you wanna start studying Git, that's the place to start. Kudos for
such practical people (more specifically Scott Charcon and Petr Baudis).

  <a href="http://git-scm.com/documentation">http://git-scm.com/documentation</a>

Just for reference, I'll paste below the stuff I had.

<blockquote>
Forwarded conversation
Subject: Git Documentation Pointers
------------------------
From: Milton Soares
Date: Mon, Jan 19, 2009 at 12:33 PM

Hi.

Just to organize and share all the references I've gathered about Git learning.

On-line Resources

 $ git help git
 $ man gittutorial

Git Homepage Resources

 <a href="http://www.kernel.org/pub/software/scm/git/docs/everyday.html">http://www.kernel.org/pub/software/scm/git/docs/everyday.html</a>
 <a href="http://www.kernel.org/pub/software/scm/git/docs/user-manual.html">http://www.kernel.org/pub/software/scm/git/docs/user-manual.html</a>

SVN to Git crash course

 <a href="http://git.or.cz/course/svn.html">http://git.or.cz/course/svn.html</a>

Recording of the Git tutorial given by Bart Trojanowski for the OGRE

 <a href="http://excess.org/article/2008/07/ogre-git-tutorial/">http://excess.org/article/2008/07/ogre-git-tutorial/</a>

Palestra Git para o Google by Linux Torvalds

 <a href="http://br.youtube.com/watch?v=4XpnKHJAok8">http://br.youtube.com/watch?v=4XpnKHJAok8</a>

Git from the bottom up

 <a href="http://www.newartisans.com/blog_files/git.from.bottom.up.php">http://www.newartisans.com/blog_files/git.from.bottom.up.php</a>

----------
From: Milton Soares
Date: Tue, Jan 20, 2009 at 4:36 PM


More pointers:

GitWine - git tips by wine people

 <a href="http://wiki.winehq.org/GitWine#head-079f5369fdb9346845a4a8c82475eb7a198312be">http://wiki.winehq.org/GitWine#head-079f5369fdb9346845a4a8c82475eb7a198312be</a>

Git Wizardry

 <a href="http://www-cs-students.stanford.edu/~blynn/gitmagic/index.html">http://www-cs-students.stanford.edu/~blynn/gitmagic/index.html</a>

Git Wiki

 <a href="http://git.or.cz/gitwiki/">http://git.or.cz/gitwiki/</a>

Git Cheat Sheet

 <a href="http://ktown.kde.org/~zrusin/git/">http://ktown.kde.org/~zrusin/git/</a>

----------
From: Milton Soares Filho
Date: Sat, Feb 21, 2009 at 11:15 AM


Yet another usefull link (pretty graphical explanations on git objects).

Tv's cobweb: Git for Computer Scientists

 <a href="http://eagain.net/articles/git-for-computer-scientists/">http://eagain.net/articles/git-for-computer-scientists/</a>
</blockquote>
