Title: Primeira Era em Manaus: Reta Final
Author: admin <milton.soares.filho@gmail.com>
Date: 2006-12-11 15:31:22

Ok, menos de duas semanas pra acabar uma das grandes fases da minha vida. Lá
pelo dia 18Dez devo estar embarcando as motos pra Belém e pegando um barco de
passageiros pra também ir até lá. Dois anos e alguns meses de Manaus prestes a
serem coroados com o início de uma grande viagem pelo Brasil.

Quem me conhece hoje sabe o quanto gosto deste lugar e o quanto este sentimento
levou pra amadurecer. No começo não era assim, o choque cultural, o trânsito
caótico, o clima, a bronca com a falta de qualidade nos serviços, o racionamento
de Matte Leão, tudo conspirava pra me fazer apertar o botãozinho de "ejetar"
dessa empreitada.

Aos poucos, depois de uns nove meses mais ou menos, a própria cultura
diferenciada, o próprio clima previsível, quase tudo que me incomodava (sim, o
trânsito e os serviços continuam incomodando, também não dá pra ser perfeito...)
foi mostrando seu lado bom. Mas o principal foi entrar em contato com tanta
gente de boa vontade e alegria contagiante. Elas me fizeram crescer, amadurecer
e preencheram com sobra o vazio de se estar longe de casa e dos amigos de
sempre. Mudaram-me bastante! Quem diria que um dia eu viraria apreciador da
Festa do Boi e teria muita vontade de aprender forró (gingado, é claro)?

Essa última eu devo pra Milena, excelente professora de dança (nas horas vagas)
e companhia melhor ainda. :-P

Bom, agora me dêem licença que ainda tenho muita coisa pra empacotar e
burocracia pra resolver tentando enviar minhas coisas de volta pra Curitiba. Em
breve postarei o endereço do blog que usaremos pra registrar a viagem desde
Belém até o sul do país.

Sucesso pra todos!
