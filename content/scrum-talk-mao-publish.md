Title: Scrum Talk @ MAO
Author: milton <milton.soares.filho@gmail.com>
Date: 2008-09-19 21:30:42

Por iniciativa da consultoria <a
href="http://www.massimus.com/english/partners.php">Massimus</a>, ocorreu em
Manaus, dia 2007-08-14 a partir das 20hs na sede da <a
href="http://www.fgvam.br/portal/modules/news/">FGV</a> uma palestra sobre
desenvolvimento ágil, com tema <em>Agile Estimating and Planning</em>.

A palestrante foi Martine Devos, já conhecida do meu tempo prestando serviço
para a BenQ-Siemens. Infelizmente os links para download dos slides no site da
consultoria estão quebrados, porém os tópicos apresentados passam pelos abaixo.

* Características do bom planejamento
* Problema da pirâmide Escopo x Prazo x Custo
* Exemplo de criação de estimativa ágil

O mais bacana dessas palestras sobre metodologias ágeis é perceber a reação da
platéia, que frequentemente espanta-se ao encarar alguma definição ou processo
totalmente diferente do que as disciplinas tradicionais de gerenciamento prezam
- gerenciamento v1.0 ou industrial, como tenho visto em alguns lugares.

Pessoalmente, chamaram-me a atenção dois pontos, um sobre convergência na
estimativa de tarefas e outro sobre mensuração de tarefas
desconhecidas/inovadoras (como as wildcards do livro do Bruce Eckel).

Na primeiro, expus minha dúvida sobre a melhor maneira de proceder quando a
equipe não chega num consenso quanto a estimativa de uma tarefa. A sugestão foi
simples e direta: estimar em equipe consiste em fazer as perguntas certas sobre
o problema atacado, de modo a quebrá-lo cada vez mais em problemas menores e
consequentemente mais fáceis de estimar. Nada de fórmula mágica ou insegurança
subjetiva, somente o bom e velho <em>dividar pra conquistar</em> que todos que
já desenvolveram software já estão cansados de ouvir falar.

No segundo, pedi orientação sobre como uma equipe novata ou com pouca
experiência no assunto do problema a resolver pode melhorar a qualidade de sua
estimativa. Achei o método apresentado fantástico. O segredo é basear-se numa
estimativa de algo conhecido - ou seja, estimativa de maior qualidade - e
confiar na capacidade do cérebro de associação. Na prática, não se diz que uma
tarefa pouco familiar X dura tantas horas, mas sim que essa mesma tarefa
<em>pode</em> durar 3 vezes o tempo da tarefa Y, que se conhece muito bem.

Vale também lembrar que as práticas ágeis permitem refinamento com o passar do
tempo, ou seja, quanto mais iterações houverem e mais histórico sua equipe
construir, maior será a qualidade de suas estimativas. Aprende-se e agrega-se
este aprendizado dentro do próprio projeto.

Parabéns ao pessoal da Massimus, Martine, Heitor Roriz e Mario Tomaselli, pela
iniciativa. Acho muito importante trazer esses conceitos de ponta pra uma região
tão cheia de projetos de software como o Pólo Industrial de Manaus, pois tenho
muita fé que essas idéias trarão uma grande melhora nas condições de trabalho
locais e na qualidade dos produtos desenvolvidos.
