Title: Silêncio no Rádio
Author: milton <milton.soares.filho@gmail.com>
Date: 2006-12-15 10:53:16

File -> Work Offline

Estou na eminência da viagem pelo litoral do Brasil, desde Belém até Porto
Alegre. A internet aqui no apartamento está pra ser desativada junto com a linha
telefônica. Isso significa ainda menos atividade aqui neste blog :-(

Mas não se preocupem, manterei todos informados a partir do site que o Thiago
preparou para a viagem.<code />

<a href="http://www.rockpesado.com.br">http://www.rockpesado.com.br</a>

Copio abaixo o warning que coloquei no scrapbook do orkut (só pra reforcar a mensagem).

<blockquote>
Povo, não espere muita interação aqui a partir de agora. Estou saindo em breve
de viagem pelo litoral brasileiro, desde Belém até o sul do país. Quem quiser
acompanhar, tentarei atualizar os blogs abaixo o máximo possível.

Blog Viagem: <a target="_blank" href="http://www.rockpesado.com.br/">http://www.rockpesado.com.br</a>
Blog Pessoal: <a target="_blank" href="http://msoares.dreamhosters.com/">http://msoares.dreamhosters.com</a>

Ah, e-mail também terá prioridade maior que scrapbook :-P

Sucesso pra todos.
</blockquote>
