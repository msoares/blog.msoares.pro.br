#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Milton Soares Filho'
SITENAME = u'Milton Soares Filho'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = u'eng'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Blog Thiago', 'http://www.tmpsantos.com.br'),
         ('Blog Jonas', 'http://amazonsecret.net'),
         ('Blog Ademar', 'http://blog.ademar.org'),
         ('Blog Sheldon', 'http://www.demario.com.br'),
         ('Blog Capitulino', 'http://blog.cpu.eti.br'),
         ('Pé na Estrada', 'http://www.rockpesado.com.br'),
        )

PLUGIN_PATHS = ['plugins']

PLUGINS = [ 'pandoc_reader' ]

# Social widget
SOCIAL = (('LinkedIn', 'https://br.linkedin.com/in/miltonsoaresfilho'),
          ('Facebook', 'https://www.facebook.com/milton.soares.filho'),
          ('G+',       'https://plus.google.com/+MiltonSoaresFilho'),
          ('Twitter',  'https://twitter.com/miltonsoaresf'),
         )

THEME = 'themes/pelican-bootstrap3'

BOOTSTRAP_THEME = 'darkly'

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
