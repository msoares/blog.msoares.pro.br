#!env python
# vim: set fileencoding=utf-8

import sys
import os.path
import codecs

from bs4 import BeautifulSoup
from pelican.utils import slugify


def find_entry(table, name, value):
    for entry in table:
        f = entry.find('field', { 'name': name })
        if f.string == value:
            return entry
    return None

def convert_sql_to_files(infile, outdir):
    with codecs.open(infile, 'r', 'utf-8') as f:
        soup = BeautifulSoup(f, 'xml')
        posts = soup.select('table_data[name=wp_posts] > row')
        metas = soup.select('table_data[name=wp_postmeta] > row')
        users = soup.select('table_data[name=wp_users] > row')

        for post in posts:
            data = dict()
            for field in post.find_all('field'):
                data[field['name']] = field.string
            data['slug'] = slugify(data['post_title'] + '-' +data['post_status'])
            outfile = os.path.join(outdir, data['slug'] + '.txt')
            print "Writing to file {}".format(outfile)
            with codecs.open(outfile, 'w', 'utf-8') as out:
                author = find_entry(users, 'ID', data['post_author'])
                meta   = find_entry(metas, 'post_id', data['ID'])

                out.write(u'Title: {}\n'.format(data['post_title']))
                out.write(u'Author: {} <{}>\n'.format(
                        author.find('field', {'name': 'user_login'}).string,
                        author.find('field', {'name': 'user_email'}).string
                    ))
                out.write(u'Date: {}\n'.format(data['post_date']))
                
                content = bytearray(data['post_content'], 'utf8').replace('\x83\xc2', '') if data['post_content'] else ''
                out.write(u'\n{}'.format(content.decode('utf8')))

if __name__ == '__main__':
    convert_sql_to_files(sys.argv[1], sys.argv[2])
